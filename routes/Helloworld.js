const express = require('express')
const router = express.Router()


const {helloWorld} = require('../controllers/Helloworld')

router.get('/', helloWorld);

module.exports = router;