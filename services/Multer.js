const multer = require('multer');


const storage = multer.diskStorage({
    destination : function(req, file, cb){
        cb(null, 'uploads/')
    },
    filename : function(req, file, cb){
        cb(null, new Date().toISOString().replace(/:/g, '-') +"_" + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg')
        cb(null,true);
    else
        cb(null,false)
}

const upload = multer({
    storage,
    /* limits : {
        fileSize : 1024 * 1024 * 5
    }, */
    fileFilter
})

const mulipleUpload = upload.fields([{name : 'image1'}, {name : 'image2', maxCount : 3}])


module.exports = {upload, mulipleUpload}