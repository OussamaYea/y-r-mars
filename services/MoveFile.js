const fs = require("fs");
const path = require("path");

const moveFile = (file , dest) => {
    try {
      if (!fs.existsSync("uploads/" + dest)){
        fs.mkdirSync("uploads/" + dest, { recursive: true });
      }
  
      const currentDest = path.basename(file);
      //console.log(currentDest);
      const targetDest =  path.resolve("uploads/" + dest, currentDest)
      //console.log(targetDest);
      fs.rename(file, targetDest, (err)=>{
          if(err) console.log(err);
          //else console.log('Successfully moved');
        });
    } catch (error) {
      console.log(error);
    }
}

module.exports = {moveFile}