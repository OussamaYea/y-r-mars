require("dotenv").config();
const express = require('express')
const app = express()

const cors = require('cors')
app.use(cors({origin:"*"}));



const bp = require('body-parser')
app.use(bp.urlencoded({extended : true}))
app.use(bp.json())

app.use(express.static('public')); 
app.use('/uploads', express.static('uploads'));




const hello_routes = require('./routes/Helloworld')
app.use('/api/', hello_routes)

app.listen(process.env.SERVER_PORT)