const helloWorld = (async (req,res) => {
    try {
        res.status(200).json("hello world");
    } catch (error) {
        res.status(500).json();
    }
})

module.exports = {helloWorld}